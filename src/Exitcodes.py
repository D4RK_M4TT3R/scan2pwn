from colorama import init, Fore
import sys

# color setup
init()
RESET = Fore.RESET
RED = Fore.RED

class ExitCode:
    def NO_OPEN_PORTS():
        print(f"{RED}[!] No open Ports found, Target could be obfuscated, try a decoy scan!{RESET}")
        print(f"{RED}[!] Exiting...{RESET}")
        sys.exit(1)

    def INVALID_PRANGE():
        print(f"{RED}[!] Invalid Port Range{RESET}")
        print(f"{RED}[!] Exiting...{RESET}")
        sys.exit(1)

    def USER_INTERRUPT():
        print(f"{RED}[!] User interrupt{RESET}")
        print(f"{RED}[!] Exiting...{RESET}")
        sys.exit(1)
    