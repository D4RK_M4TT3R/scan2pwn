import ssl, socket
import sys
import random
import socket, threading
from CVE_APIS import Exploit_API
from pyfiglet import figlet_format
from threading import Thread, Lock
from queue import Queue
from colorama import init, Fore
from logging import getLogger, ERROR
from datetime import datetime
from termcolor import cprint
from Exitcodes import ExitCode
import ipaddress

# color setup
init()
GREEN = Fore.GREEN
RED = Fore.RED
YELLOW = Fore.YELLOW
BLUE = Fore.LIGHTBLUE_EX
RESET = Fore.RESET
CYAN = Fore.CYAN
ORANGE = Fore.MAGENTA

# threads and queue
queue = Queue()
thread_lock = Lock()

#http ports
_HTTP_PORTS = [80, 443, 8080, 8000]

# Open Ports
open_ports = []

class PortScanner:
    def __init__(self, host, n_threads, decoys=None, output_file=None):
        self.HOST = host
        self.NUM_THREADS = n_threads
        self.decoys = self.decoy_generator(decoys)

    def threading(self):
        global queue
        while True:
            worker = queue.get()
            if self.decoys != None:
                self.scan_ports(worker, random.choice(self.decoys))
            else:
                self.scan_ports(worker)
            queue.task_done()

    def scan_ports(self, port, decoy="192.168.0.140"):
        try:
            socket.setdefaulttimeout(2)
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            # TODO: Use decoys
            # if self.decoys != None:
                # s.bind(str(decoy), 5000)
            s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            
            s.connect((self.HOST, port))
            try:
                service = socket.getservbyport(port)
            except:
                service = f"{RED}not recognised{RESET}"
        except:
            with thread_lock:
                pass
        else:
            with thread_lock:
                print(f"{CYAN}[INFO]{RESET} PORT: {YELLOW}{port:5}{RESET} | STATUS: {GREEN}OPEN{RESET} | SERVICE: {BLUE}{service:5}{RESET}")
                open_ports.append(port)
        finally:
            s.close()

    def main(self, ports):
        self.create_ascii_art()
        ## Port Scan
        print(f"{GREEN}[+++] {YELLOW}Initiating Port Scan...{RESET}")
        global queue
        start_time = datetime.now()
        for thread in range(self.NUM_THREADS):
            thread = Thread(target=self.threading)
            thread.daemon = True
            thread.start()

        for worker in ports:
            queue.put(worker)
        queue.join()

        if len(open_ports) == 0:
            ExitCode.NO_OPEN_PORTS()
        ## Banner grabbing 
        print(f"\n{GREEN}[+++] {YELLOW}Initiating Service Scan...{RESET}")
        
        ## Services list for later exploit detection
        services_list = []
        
        for port in open_ports:
            service = self.banner_grabbing(self.HOST, int(port))
            services_list.append(service)

        self.search_exploits(services_list)    

        end_time = datetime.now()
        total_scan_time = end_time-start_time
        print(f"\nTotal Scan Time = {BLUE}{total_scan_time}{RESET}")

    # Simple banner grabbing 
    # TODO FIX double output detected / non detected --> returned version always None 
    def banner_grabbing(self, ip, port):
        socket.setdefaulttimeout(2)
        bannergrabber = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            ## Grab possible subdomains from ssl cert
            if port == 443:
                ctx = ssl.create_default_context()
                with ctx.wrap_socket(bannergrabber) as s:
                    s.connect(ip, 443)
                    cert = s.getpeercert()
                subject = dict(x[0] for x in cert['subject'])
                issued_to = subject['commonName']
                s.close()

            bannergrabber.connect((ip,int(port)))
            if(port in _HTTP_PORTS):
                bannergrabber.send(b'GET HTTP/1.1\r\n')
                banner = bannergrabber.recv(4096)

            else:
                bannergrabber.send(b'WhoAreYou\r\n')
                banner = bannergrabber.recv(100)

            version = self.filter_service_version(banner)
            bannergrabber.close()

            # only output the actual service version
            print(f"{CYAN}[INFO]{RESET} PORT: {YELLOW}{port:5}{RESET} | SERVICE-VERSION: {BLUE}{version:5}{RESET}")
            # Output whole response 
            print(f"{ORANGE}[-----SERVICE-INFO-----]\n{RESET}{banner.decode('UTF-8')}")
            if len(issued_to) != 0:
                print(f"[{GREEN}+{RESET}] SSL Cert found: {issued_to}")
            return version
        except:
            version=None
            print(f"{CYAN}[INFO]{RESET} PORT: {YELLOW}{port:5}{RESET} | SERVICE-VERSION: {RED}Not detected {RESET}")
            return version

        # return version
    
    def decoy_generator(self, ip_range):
        if ip_range != None:
            print(f"{GREEN}[+++] {YELLOW}Creating IP decoys...{RESET}")
            ip_decoys = [str(ip) for ip in ipaddress.IPv4Network("192.168.0.0/24")]
            return ip_decoys


    def check_ip_area(self, ip):
        pass
        # TODO Check if scanned adress is in private network or public ip and create decoys

    def filter_service_version(self, response):
        service = response.decode('UTF-8')
        for line in service.split("\n"):
            if "Server: " in line:
                return line.lstrip("Server: ")
            elif "<adress>" in line:
                return line.lstrip("<adress>").rstrip("</adress>")
        return service.rstrip()

    def create_ascii_art(self):
        # Ascii Art
        cprint(figlet_format('Scan2Pwn', font='graffiti'),'red', attrs=['bold'])
        # Creators
        print(f"\tby {BLUE}@D4RK_M4TT3R{RESET} and {BLUE}@shellcurity{RESET}\n")

    def check_OS(self):
        # Check ssh if active for revealing OS
        pass

    def search_exploits(self, services):
        # only output the actual service version
        print(f"\n{GREEN}[+++] {YELLOW}Initiating Exploit Detection...{RESET}")
        for service in services:
            if service != None:
                Exploit_API = Exploit_API(service)
                exploits = Exploit_API.get_exploits()
                if len(exploits) == 0:
                    print(f"{CYAN}[INFO]{RESET} Service: {YELLOW}{service}{RESET}\n POSSIBLE EXPLOITS: {RED}None detected{RESET}")    
                else:
                    print(f"{CYAN}[INFO]{RESET} Service: {YELLOW}{service}{RESET}\n POSSIBLE EXPLOITS: {BLUE}{5:exploits}{RESET}")