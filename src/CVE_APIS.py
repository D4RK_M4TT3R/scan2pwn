from pprint import pprint
import requests
import json

class Exploit_API:
    def __init__(self, service):
        self.searchterm = service
        self.__url = "https://services.nvd.nist.gov/rest/json/cves/1.0" 
        self.PARAMS = { "keyworkd": self.searchterm}
# __url = "https://services.nvd.nist.gov/rest/json/cves/1.0"
## Identified service and version will be added as searchtermn
# searchterm = "Apache 5.16.1"
    def get_exploits(self):
        req = requests.get(self.__url, params=self.PARAMS)
        data = req.json()

        cve_list = []
        for cve_items in data['result']['CVE_Items']:
            cve_list.append(cve_items['cve']['CVE_data_meta']['ID'])

        return cve_list

# PARAMS = {
#     "keyword": searchterm,
#         }

# r = requests.get(url=__url, params=PARAMS)
# print(r.status_code)
# data = r.json()

# cves = []
# # Saving all CVE's for the searchterm into cves list
# for cve_items in data['result']['CVE_Items']:
#     cves.append(cve_items['cve']['CVE_data_meta']['ID'])

#print("All found CVE's for {}: {}".format(searchterm,cves))
