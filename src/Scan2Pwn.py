from PortScanner import PortScanner
import argparse
import sys
from colorama import init, Fore
from Exitcodes import ExitCode


# color setup
init()
GREEN = Fore.GREEN
RED = Fore.RED
YELLOW = Fore.YELLOW
BLUE = Fore.LIGHTBLUE_EX
RESET = Fore.RESET

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
    description=f'''{RED}Vulnerability Scanner{RESET} by {BLUE}@D4RK_M4TT3R{RESET} and {BLUE}@shellcurity{RESET}\n
                    | {YELLOW}Use this Scanner wisely and stay on the{RESET} white{YELLOW} side!{RESET}''')

    parser.add_argument('--hidden',dest="decoy_range", help=f'{YELLOW}Execute hidden Scan with IP Decoys -- Add Adress range for example 192.168.0.0/24{RESET}')
    parser.add_argument('-u', dest="url", help=f'{YELLOW}Target URL{RESET}')
    parser.add_argument('-ip',dest="ip", help=f'{YELLOW}Target IP-Adress{RESET}')
    parser.add_argument('-p', dest="port_range", default="1-10000", help=f'{YELLOW}Port range -- Default = 1-10000 , -p- = all ports, -p 1000 = single port{RESET}')
    parser.add_argument('-udp', dest="udp", action="store_true", help=f'{YELLOW}Perform UDP Scan instead of the default TCP{RESET}')
    parser.add_argument('-ad',dest="av_detection", help=f'{YELLOW}Use AV Detection{RESET}')
    parser.add_argument('-od',dest="os_detection", help=f'{YELLOW}Use OS Detection{RESET}')
    parser.add_argument('-o',dest="output_file", help=f"{YELLOW}Output to file -o /path/filename{RESET}")
    parser.add_argument('-t', dest="threads", default="1000", help=f"{YELLOW}Number of Threads -- Default = 1000 | {RED}WARNING: Know your systems limits!{RESET}")
    
    # If no arguments print help
    if len(sys.argv) < 2:
        parser.print_help()
        sys.exit(1)
    args=parser.parse_args()
    
    # Passed Args
    decoy_range = args.decoy_range
    ip = args.ip
    port_range = args.port_range
    threads = args.threads
    url = args.url
    out_file = args.output_file
    threads = int(threads)

    # Max Port range
    ports = []
    s_port = 0
    e_port = 65635
    
    # Handle inputs for Ports
    if port_range == "-":
        ports = [ port for port in range(s_port, e_port)]
    elif "-" not in port_range:
        ports.append(int(port_range))
    elif "-" in port_range:
        s_port, e_port = port_range.split("-")
        s_port, e_port = int(s_port), int(e_port)
        if s_port >= 0 and e_port >= 0 and e_port >= s_port:
            pass
        else:
            ExitCode.INVALID_PRANGE()
        ports = [ port for port in range(s_port, e_port+1)]

    # Setting Target URL or IP
    target_host = ""
    if ip != None and url == None:
        target_host = ip
    if url != None and ip == None:
        target_host = url

    # Setting UDP Scan TODO
    udp_scan = None
    if args.udp != None:
        udp_scan = args.udp
    
    # Setting up decoy range 
    if decoy_range != None:
        Scanner = PortScanner(target_host, threads, decoy_range)
    else:
        Scanner = PortScanner(target_host, threads)

    try:
        Scanner.main(ports)
    except KeyboardInterrupt:
        ExitCode.USER_INTERRUPT()
