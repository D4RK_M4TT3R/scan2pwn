<img src="http://www.hackthebox.eu/badge/image/296840" alt="Hack The Box"> 

# In Developement Vulnerability Scannner 

**Screenshots**
![](Images/Screenshot_Help.png)
![](Images/Screenshot_Scan.png)


**Main Lang:** Python-3

This Project is mainly developed for a learning purpose. The goal of the application is to offer a fast port scanner with multiple options like service, OS, AV detections and based on that a cve api will give exploit (CVE) suggestion that may work on the target as well as general vulnerabilities that should further be investigated!

## TODO's:
- Improve Port detection and probing
- Decoy Scan option
- AV Detection
- OS Detection
- Combine with CVE api
- Automatically create a full report on all discovered vulnerabilities

## Software Security:
- Review Vulnerabilities after every push/merge Request 
- If pipeline (Bandit) failed, check why and fix the issue

